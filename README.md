# txAdminRecipe

**Description:** Complete RP Server For FiveM based on QBCore. 

This recipe runs inside [**txAdmin**](https://github.com/tabarra/txAdmin).  
Please check the [**Recipe Documentation Page**](https://github.com/tabarra/txAdmin/blob/master/docs/recipe.md).

to untrack files you dont want to show up in git use the following command
i do this in the beginning of forking the repo as to always make sure i have a basic template file to still be used

```
git update-index --assume-unchanged path/to/file
```